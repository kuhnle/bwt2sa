#ifndef BWT2SA_UTIL
#define BWT2SA_UTIL
#define BYTESINT 5
#include "lib/rle_string.hpp"
#include "lib/kwaymergesort.h"

// a basic struct for a Pair entry
// This struct is for use with the
// external merge sort, sorting is by first
// entry only

struct Pair {
  uint64_t first;
  uint64_t second;
    
    bool operator < (const Pair &b) const
    {
        if      (first < b.first)  return true;
        else return false;
    }
    
    // overload the << operator for writing a Pair struct
    friend ostream& operator<<(ostream &os, const Pair &b) 
    {
       //os  << b.first  << " " 
       //<< b.second  << " ";
       os << b.second;

        return os;
    }
    // overload the >> operator for reading into a Pair struct    
    friend istream& operator>>(istream &is, Pair &b) 
    {
      //is  >> b.first 
      //>> b.second;
      is.read( (char*) &(b.first), BYTESINT );
      is.read( (char*) &(b.second), BYTESINT );
      return is;
    }

   void full_write( std::ostream& os ) const {
      os << first << ' ' << second << ' ';
   }
};

// comparison function for sorting Pairs, then by start.
bool byFirst(Pair const &a, Pair const &b) {
    return (a.first) < (b.first);
}

template <typename T>
std::vector<std::size_t> sort_permutation(
					  const std::vector<T>& vec )
{
  std::vector<std::size_t> p(vec.size());
  std::iota(p.begin(), p.end(), 0);
  std::sort(p.begin(), p.end(),
	    [&](std::size_t i, std::size_t j){ return vec[i] < vec[j]; });
  return p;
}

template <typename T, typename Compare>
std::vector<std::size_t> sort_permutation(
					  const std::vector<T>& vec,
					  Compare& compare)
{
  std::vector<std::size_t> p(vec.size());
  std::iota(p.begin(), p.end(), 0);
  std::sort(p.begin(), p.end(),
	    [&](std::size_t i, std::size_t j){ return compare(vec[i], vec[j]); });
  return p;
}

template <typename T>
void apply_permutation_in_place(
				std::vector<T>& vec,
				const std::vector<std::size_t>& p)
{
  std::vector<bool> done(vec.size());
  for (std::size_t i = 0; i < vec.size(); ++i)
    {
      if (done[i])
	{
	  continue;
	}
      done[i] = true;
      std::size_t prev_j = i;
      std::size_t j = p[i];
      while (i != j)
	{
	  std::swap(vec[prev_j], vec[j]);
	  done[j] = true;
	  prev_j = j;
	  j = p[j];
	}
    }
}
#endif
