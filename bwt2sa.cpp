
#include <iostream>
#include <unistd.h>
#include <fstream>
#include <chrono>

#include "util.hpp"

using namespace std;
using namespace ri;

//typedef rle_string_sd bwttype;
//typedef rle_string_hyb bwttype;
typedef huff_string bwttype;

struct Args {
   string bwtFileName = "";
   string outFileName = "";
   string Bwt;
   bwttype* bwt;
  uint8_t charTerminator = 255;
  uint8_t charSubTerminator = 3;
  size_t posTerminator = 0;
  vector< size_t > pos_sub_terminator;
   size_t nThreads = 1;
   size_t n;
   uint64_t* SA;
   uint64_t* LF;
   vector< uint64_t > SAsamp;
   vector< uint64_t > C;
   bool bPrint = false;
   bool smallLF = true;
   bool fullSA = false;
   bool dryRun = false;
   bool bBin = false;
   int bufferSize = 100; //buffer size in megabytes, default: 100 MB
};

void print_help(char** argv) {
   cout << "Usage: " << argv[ 0 ] << " <bwt filename> <output filename> [options]" << endl << endl;
   cout << "Options: " << endl
	<< "-f\t\tcompute full suffix array instead of sampled" << endl
	<< "-M [size]\tMax. memory in MB to use for external sorting (full SA only, default 100 MB)" << endl;
   exit(1);
}

void parseArgs( int argc, char** argv, Args& arg ) {
   int c;
   extern char *optarg;

   string sarg;
   while ((c = getopt( argc, argv, ":B:x:hpldfbM:") ) != -1) {
      switch(c) {
      case 'M':
	 sarg.assign( optarg );
	 arg.bufferSize = stoi( sarg );
	 break;
      case 'b':
	 arg.bBin = true;
	 break;
      case 'l':
	 arg.smallLF = false;
	 break;
      case 'f':
	 arg.fullSA = true;
	 break;
      case 'x':
	 sarg.assign( optarg );
	 arg.nThreads = stoi( sarg );
	 break;
      case 'B':
	 sarg.assign( optarg );
	 arg.bwtFileName = sarg;
	 break;
      case 'p':
	 arg.bPrint = true;
	 break;
      case 'd':
	 arg.dryRun = true;
	 break;
      case '?':
	 cout << "Unknown option. Use -h for help." << endl;
	 exit(1);
	 break;
      case 'h':
	 print_help(argv);
	 break;
      }
   }

   if (argc == optind) {
      cout << "No filename for bwt given, so nothing to do (use -h for usage info)." << endl;
      exit( 1 );
   }
   
   arg.bwtFileName.assign( argv[optind] );

   if (argc == optind + 1) {
      cout << "No output filename..." << endl;
      if (arg.fullSA)
	 arg.outFileName = arg.bwtFileName + ".sa";
      else
	 arg.outFileName = arg.bwtFileName + ".ssa";
      return;
   }

   arg.outFileName.assign( argv[optind + 1] );
}

void loadBwt( Args& args ) {
   string bwt;
   cout << "Reading bwt from file \'" << args.bwtFileName << "\'..."; cout.flush();
   ifstream in( args.bwtFileName.c_str() , ios::in | ios::binary );
   bwt.assign((std::istreambuf_iterator<char>(in)),
	       std::istreambuf_iterator<char>());
   args.n = bwt.size();
   cout << "done." << endl;

   //Valid chars are 0...255.
   vector< uint64_t > counts( 256, 0 );
   
   cout << "Computing C array..."; cout.flush();
   for (size_t i = 0; i < bwt.length(); ++i) {
      if (bwt[i] == 0)
	 bwt[i] = bwt[i] + 1;
      ++counts[ (unsigned int) bwt[i] ];
      if ( bwt[i] < args.charTerminator ) {
	 args.posTerminator = i;
	 args.charTerminator = bwt[i];
      }
      if ( bwt[i] == args.charSubTerminator ) {
	args.pos_sub_terminator.push_back( i );
      }
   }
   
   if (counts[ args.charTerminator ] > 1) {
      cout << "Error: input BWT has more than one terminator. ";
      cout << "This may have been caused by the presence of 0 and 1 characters in the string, which is not allowed.\n";
      exit(1);
   }
   
   vector< uint64_t >& C = args.C;
   C.assign( 256, 0 );
   C[0] = 0; //0 is lexicographically least
   for (size_t i = 1; i < 256; ++i) {
      C[i] = C[ i - 1 ] + counts[ i - 1 ];
   }
   cout << "done." << endl;
   
   if (args.smallLF) {
      cout << "Compressing bwt..."; cout.flush(); 
      args.bwt = new bwttype( bwt );
      cout << "done." << endl;
   } 

   if (!args.smallLF) {
      args.Bwt.swap( bwt );
   }
}

size_t LF( Args& args, size_t i ) {
   bwttype& bwt = *(args.bwt);
   return args.C[ (unsigned) bwt[i] ] + bwt.rank( i, bwt[i]);
}

void computeFullLF( Args& args ) {
   args.LF = new uint64_t[ args.n ];
   vector< uint64_t > counts( args.C.begin(), args.C.end() );
   
   for (size_t i = 0; i < args.n; ++i) {
      uchar c = args.Bwt[i];
      ++(counts[ (unsigned) c ]);
      args.LF[i] = counts[ (unsigned) c ] - 1;
   }
}

void computeSampSA( Args& args ) {
   //ofstream offirst;
   //ofstream oflast;
   //string tmp = args.outFileName + ".first";
   //offirst.open( tmp.c_str() );
   //tmp = args.outFileName + ".last";
   //oflast.open( tmp.c_str() );

   vector <uint64_t> pos; //j
   vector <uint64_t>& val = args.SAsamp; //SA[j]
   
   cout << "Computing SA sample..."; cout.flush();
   auto t1 = chrono::high_resolution_clock::now();
   
   size_t j = args.posTerminator;
   size_t& n = args.n;
   bwttype& bwt = *(args.bwt);
   for (size_t i = n; i > 0; --i) {
      if ( j == n - 1 ) {
	 //last character of bwt
	 //is last char in a run.
	 pos.push_back( j );
	 val.push_back( i % n );
	 //oflast << j << ' ' << i % n << '\n';
      } else {
	 if (bwt[j] != bwt[j + 1]) {
	    //last character of run
	    pos.push_back( j );
	    val.push_back( i % n );
	    //oflast << j << ' ' << i % n << '\n';
	 }
      }

      //For now, only compute last characters of
      //run 
      // if (j == 0 ) {
      // 	 //first character of bwt
      // 	 //is first char in a run
      // 	 offirst << j << ' ' << i % n << '\n';
      // } else {
      // 	 if (bwt[j] != bwt[j-1]) {
      // 	    //first character of run
      // 	    offirst << j << ' ' << i % n << '\n';
      // 	 }
      // }

      j = LF( args, j );
   }
   auto t2 = chrono::high_resolution_clock::now();
   cout << "done." << endl;
   
   cout << "sorting sample..."; cout.flush();
   auto t3 = chrono::high_resolution_clock::now();
   auto p = sort_permutation( pos );
   apply_permutation_in_place( val, p );
   auto t4 = chrono::high_resolution_clock::now();
   cout << "done." << endl;
   cout << "Writing to file..."; cout.flush();
   ofstream of( args.outFileName.c_str() , ios::out | ios::binary );
   for (size_t i = 0; i < val.size(); ++i) {
     //of << val[i] << '\n';
     of.write( (char*) &(val[i]) , BYTESINT );
   }
   //of.write( (char*) &(val[0]) , val.size()*sizeof(uint64_t) );
   of.close();
   cout << "done." << endl;

   uint64_t sec_computeSA = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
   uint64_t sec_sortSA = std::chrono::duration_cast<std::chrono::milliseconds>(t4 - t3).count();

   
   cout << "1. Computing SA (s): " << sec_computeSA / 1000.0 << endl;
   cout << "2. Sorting SA (s): " << sec_sortSA / 1000.0 << endl;
   cout << "Total time (s): " << (sec_computeSA + sec_sortSA) / 1000.0 << endl;

}

void writepair( uint64_t j, uint64_t i, ofstream& of ) {
   of.write( (char*) &j, sizeof(j) );
   of.write( (char*) &i, sizeof(i) );
}

void computeSampSAbin( Args& args ) {
   ofstream offirst;
   ofstream oflast;
   string tmp = args.outFileName + ".first";
   offirst.open( tmp.c_str(), ios::binary | ios::out );
   tmp = args.outFileName + ".last";
   oflast.open( tmp.c_str(), ios::binary | ios::out );
   
   cout << "Computing SA sample..."; cout.flush();
   size_t j = args.posTerminator;
   size_t& n = args.n;
   bwttype& bwt = *(args.bwt);
   for (size_t i = n; i > 0; --i) {
      if ( j == n - 1 ) {
	 //last character of bwt
	 //is last char in a run.
	 writepair( j, i % n, oflast );
      } else {
	 if (bwt[j] != bwt[j + 1]) {
	    //last character of run
	    writepair( j, i % n, oflast );
	 }
      }
	 
      if (j == 0 ) {
	 //first character of bwt
	 //is first char in a run
	 writepair(j, i % n, offirst );

      } else {
	 if (bwt[j] != bwt[j-1]) {
	    //first character of run
	    writepair(j, i % n, offirst );
	 }
      }

      j = LF( args, j );
   }
   
   cout << "done." << endl;
}

void compute_full_SA_parallel( Args& args ) {
  
}

void computeFullSA( Args& args ) {
   string fnameUnSrt = args.outFileName + ".unsrt";
   ofstream of( fnameUnSrt.c_str() , ios::out | ios::binary );
   //cout << sizeof(uint64_t) * 2 * args.n / 1024.0 / 1024.0 << endl;
   
   cout << "Computing full SA..."; cout.flush();

   auto t1 = chrono::high_resolution_clock::now();

   uint64_t j = args.posTerminator;
   size_t& n = args.n;

   
   //args.SA[ j ] = 0;
   //of << j << ' ' << 0 << '\n';
   uint64_t zero = 0;
   of.write( (char*) &j, BYTESINT );
   of.write( (char*) &zero, BYTESINT );
      
   j = LF( args, j );
   for (uint64_t i = n - 1; i > 0; --i) {
      //args.SA[ j ] = i;
     //of << j << ' ' << i << '\n';
     of.write( (char*) &j, BYTESINT );
     of.write( (char*) &i, BYTESINT );
     j = LF(args, j );
   }

   of.close();
   auto t2 = chrono::high_resolution_clock::now();
   cout << "done." << endl;
   
   cout << "Sorting SA externally..."; cout.flush();
   auto t3 = chrono::high_resolution_clock::now();
   int bufferSize = args.bufferSize * 1024 * 1024;
   bool compressOutput = false;       // not yet supported
   string tempPath     = "./";        
   ofstream off( args.outFileName.c_str(), ios::out | ios::binary );
   
   // sort the Pair file 
   KwayMergeSort<Pair> *bed_sorter = new KwayMergeSort<Pair> ( fnameUnSrt, 
							       &off, 
							       bufferSize, 
							       compressOutput, 
							       tempPath);

   bed_sorter->SetComparison( byFirst );
   bed_sorter->Sort();

   auto t4 = chrono::high_resolution_clock::now();
   off.close();
   cout << "done." << endl;
   uint64_t sec_computeSA = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
   uint64_t sec_sortSA = std::chrono::duration_cast<std::chrono::milliseconds>(t4 - t3).count();

   
   cout << "1. Computing SA (s): " << sec_computeSA / 1000.0 << endl;
   cout << "2. Sorting SA (s): " << sec_sortSA / 1000.0 << endl;
   cout << "Total time (s): " << (sec_computeSA + sec_sortSA) / 1000.0 << endl;
}

void computeFullSAnoSort( Args& args ) {
   string fnameUnSrt = args.outFileName + ".unsrt";
   ofstream of( args.outFileName.c_str() , ios::out | ios::binary );
   //cout << sizeof(uint64_t) * 2 * args.n / 1024.0 / 1024.0 << endl;

   //prepare output file
   size_t fileSize = BYTESINT * args.n;
   uint64_t zero = 0;
   for (size_t i = 0; i < args.n; ++i) {
     of.write( (char*) &zero, BYTESINT );
   }
   
   cout << "Computing full SA..."; cout.flush();

   auto t1 = chrono::high_resolution_clock::now();

   uint64_t j = args.posTerminator;
   size_t& n = args.n;

   
   //args.SA[ j ] = 0;
   //of << j << ' ' << 0 << '\n';

   of.seekp( j * BYTESINT, ios::beg );
   //of.write( (char*) &j, BYTESINT );
   of.write( (char*) &zero, BYTESINT );
      
   j = LF( args, j );
   for (uint64_t i = n - 1; i > 0; --i) {
      //args.SA[ j ] = i;
     //of << j << ' ' << i << '\n';
     of.seekp( j * BYTESINT, ios::beg );
     //of.write( (char*) &j, BYTESINT );
     of.write( (char*) &i, BYTESINT );
     j = LF(args, j );
   }

   of.close();
   auto t2 = chrono::high_resolution_clock::now();
   cout << "done." << endl;
   
   uint64_t sec_computeSA = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();

   
   cout << "1. Computing SA (s): " << sec_computeSA / 1000.0 << endl;

}

void printSA( Args& args ) {
   cout << "SA sample: ";
   for (size_t i = 0; i < args.SAsamp.size(); ++i) {
      cout << args.SAsamp[i] << ' ';
   }
   cout << endl;
   
}

int main( int argc, char** argv ) {
   Args args;
   
   parseArgs( argc, argv, args );

   loadBwt( args );
   if (args.fullSA)
     computeFullSA( args );
   else {
      computeSampSA( args );
   }

   return 0;
}
