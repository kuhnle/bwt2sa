# BWT2SA

## Dependencies
- [Succinct Data Structure Library](https://github.com/simongog/sdsl-lite) of Simon Gog
  
- The GNU C++ compiler, `g++` version `4.9.2`.

## Compilation
Tested with `g++ 4.9.2`, with the Succinct Data Structure Library installed
to `/usr/local/`. Update the Makefile to include the location
of the SDSL library, if necessary. Then, `make` produces the executable `bwt2sa`.

## Usage
```
Usage: bwt2sa <bwt filename> (<output filename>) [options]

Options:
-f		compute full suffix array instead of sampled
-M [size]	Max. memory in MB to use for external sorting (full SA only)
```
By default, `bwt2sa` computes the sample of the suffix array
required for the r-index. Flag `-f` causes the full suffix array
to be computed instead. 
### Required parameter `<bwt filename>`
This parameter is the name of the input file
containing the Burrows-Wheeler Transform of
the string for which the (sampled) suffix array is to
be computed. The end-of-string symbol is interpreted to
be the lexicographically least character in the file,
which must be unique.
Also, it is not allowed to have both
0x00 and 0x01 characters in the file: if 0x00 is
the end-of-string symbol, the next smallest character must be 0x02. 
### Required parameter `<output filename>`
The sampled suffix array is written to the file `<output filename>`,
one entry per line. 

If option `-f` is used,
the full suffix array is written to the file
instead.

### Examples
Using the provided BWT `banana.txt.bwt` of `banana.txt`, compute the
full suffix array for `banana.txt`:
```
$ ./bwt2sa banana.txt.bwt banana.fsa -f
$ cat banana.fsa
6
5
3
1
0
4
2
```

