# compilation flag
CXX=g++
CXX_FLAGS=-std=c++11 -O3 -Wall -Wextra -DNDEBUG -I/s/apu/a/homes/kuhnle/include -L/s/apu/a/homes/kuhnle/lib
DEBUG_FLAGS=-std=c++11 -O0 -g -Wall -Wextra
EXECS=bwt2sa

all: $(EXECS)

bwt2sa: bwt2sa.cpp 
	$(CXX) $(CXX_FLAGS) -o $@ $^ -lsdsl
clean:
	rm $(EXECS)
debug: bwt2sa.cpp
	$(CXX) $(DEBUG_FLAGS) -o $@ $^ -lsdsl
